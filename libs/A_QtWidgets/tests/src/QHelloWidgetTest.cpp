#include "Mdt/A/QHelloWidget"
#include <QApplication>
#include <QTimer>

int main(int argc, char **argv)
{
  QApplication app(argc, argv);

  Mdt::A::QHelloWidget widget;
  widget.show();

  QTimer::singleShot(5000, &widget, &Mdt::A::QHelloWidget::close);

  return app.exec();
}
