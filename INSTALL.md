
This section explains how to build and install _Project_name_.

[[_TOC_]]

# Required tools and libraries

The tools and libraries described in [README](README.md)
are also required here.

# Build and install _Project_name_

This section will summarise how to build and install _Project_name_ .

For more explanations you should take a look at [BUILD](BUILD.md).

Get _Project_name_:
```bash
git clone git@gitlab.com:scandyna/_project_name_.git
```

Create a build directory and cd to it:
```bash
mkdir build
cd build
```

## Note about install prefix

Some note on the `CMAKE_INSTALL_PREFIX`:
 - To target a system wide installation on Linux, set it to `/usr` (`-DCMAKE_INSTALL_PREFIX=/usr`) .
 - For other locations, spcecify also the `<package-name>`, (for example `-DCMAKE_INSTALL_PREFIX=~/opt/_Project_name_`).

For details about that, see:
 - https://scandyna.gitlab.io/mdt-cmake-modules/Modules/MdtInstallDirs.html
 - https://cmake.org/cmake/help/latest/variable/CMAKE_INSTALL_PREFIX.html
 - https://scandyna.gitlab.io/mdt-cmake-modules/Modules/MdtInstallLibrary.html


## Build and install on Linux with the native compiler

Install the dependencies:
```bash
conan install -s build_type=Release --build=missing ..
```

Configure _Project_name_:
```bash
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=~/opt/_Project_name_ ..
cmake-gui .
```

Example to use Qt 5.14.2 not installed system wide:
```bash
cmake -DCMAKE_BUILD_TYPE=Release -DQT_PREFIX_PATH="~opt/qt/Qt5/5.14.2/gcc_64/" -DCMAKE_INSTALL_PREFIX="~/opt/_Project_name_" ..
cmake-gui .
```

To build and install, run:
```bash
cmake --build . --target INSTALL
```

## Build and install on Windows MinGW

Open a terminal that has gcc and mingw32-make in the PATH.

Create a build directory and go to it:
```bash
mkdir build
cd build
```

Install the required dependencies:
```bash
conan install --profile windows_gcc7_x86_64 -s build_type=Release --build=missing ..
```

Activate the build environment:
```bash
.\activate
```

Configure the project:
```bash
cmake -G"MinGW Makefiles" -DCMAKE_INSTALL_PREFIX=C:\some\path -DCMAKE_BUILD_TYPE=Release ..
cmake-gui .
```

If `QT_PREFIX_PATH` was not specified, and no Qt5 installation is in the `PATH`,
a error will probably occur, telling that Qt was not found.
Set the `QT_PREFIX_PATH` by choosing the path to the Qt5 installation,
then run "Configure".

Also choose different options, like the components to build.
Once done, run "Generate", then quit cmake-gui.

To build and install, run:
```bash
cmake --build . --target INSTALL
```

To restore the standard environment:
```bash
.\deactivate
```

# Create a Conan package

The package version is picked up from git tag.
If working on _Project_name_, go to the root of the source tree:
```bash
git tag x.y.z
conan create . scandyna/testing --profile $CONAN_PROFILE -s build_type=$BUILD_TYPE
```

To create a package without having a git tag:
```bash
conan create . x.y.z@scandyna/testing --profile $CONAN_PROFILE -s build_type=$BUILD_TYPE
```

Above examples will generate a package that uses the Qt version that is installed on the system,
or passed to the `CMAKE_PREFIX_PATH` of your build.

To create packages that depend on Conan Qt:
```bash
conan create . scandyna/testing -o _Project_name_:use_conan_qt=True
```

Because Qt offers binary compatibility,
it should not be required to create package for each minor Qt version,
but more a package per compiler and other things that breaks binary compatibility.
