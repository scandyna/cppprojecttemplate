# _Project_name_

Description

# Usage

For the available classes, functions, and their usage,
see [the API documentation](https://scandyna.gitlab.io/projectname)

This section will explain how to build your project using _Project_name_
with CMake and Conan.

## Required tools and libraries

Some tools and libraries are required to use _Project_name_:
 - Git
 - CMake
 - Conan (optional)
 - A compiler (Gcc or Clang or MSVC)
 - Make (optional)
 - Qt5
 - [boost](https://www.boost.org)
 - Doxygen (optional, only to build the C++ API documentation)

For a overview how to install them, see https://gitlab.com/scandyna/build-and-install-cpp

Qt5 could be managed by Conan using [conan-qt](https://github.com/bincrafters/conan-qt),
but this does not allways work,
and building Qt if the required binaries are not available can be long.
This is why a option to use a non Conan managed Qt is provided.

Required tools and libraries that can be managed by Conan:
 - [mdt-cmake-modules](https://gitlab.com/scandyna/mdt-cmake-modules)


If you use Conan, _Project_name_ and its dependencies that are managed by Conan
are not required to be installed explicitly.
Otherwise see [INSTALL](INSTALL.md).

## conanfile.txt

Create (or update) `conanfile.txt`:
```txt
[requires]
_Project_name_/[>=x.y.z]@scandyna/testing

[generators]
cmake
virtualenv
```

## CMake project description

Update your CMakeLists.txt to use the required libraries:
```cmake
cmake_minimum_required(VERSION 3.10)
project(MyApp)

if(EXISTS "${CMAKE_BINARY_DIR}/conanbuildinfo.cmake")
  include("${CMAKE_BINARY_DIR}/conanbuildinfo.cmake")
  conan_basic_setup(NO_OUTPUT_DIRS)
endif()

find_package(Threads REQUIRED)
find_package(Qt5 COMPONENTS Core Gui REQUIRED)
find_package(Mdt0 COMPONENTS _Project_Lib_Name_ REQUIRED)

add_executable(myApp myApp.cpp)
target_link_libraries(myApp Mdt0::_Project_Lib_Name_)
```

Note that we include `conanbuildinfo.cmake` and call `conan_basic_setup()`.
This is the only chage required in the top-level CMakeLists.txt .
All the rest still the same.
`conan_basic_setup()` will set `CMAKE_PREFIX_PATH`,
so `find_package()` will locate the dependencies properly.
And, also important: this function also updates some flags,
for example to use the correct standard library
(libc++, libstdc++, libstdc++11).


## Build your project using _Project_name_ on Linux with the native compiler

Create a build directory and cd to it:
```bash
mkdir build
cd build
```

Install the dependencies:
```bash
conan install -s build_type=Release --build=missing ..
```

Configure your project:
```bash
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake-gui .
```

Build your project:
```bash
make -j4
```

# Install _Project_name_

To install _Project_name_ see [INSTALL](INSTALL.md).

# Work on _Project_name_

To build _Project_name_ and run the unit tests, see [BUILD](BUILD.md).
