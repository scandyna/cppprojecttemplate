
# CMake

ENABLE_TESTS
ENABLE_BENCHMARKS
ENABLE_EXAMPLES

or

BUILD_TESTS
BUILD_BENCHMARKS
BUILD_EXAMPLES


# Conan

test_package

see remotes.txt

## Conant install

build_tests option must not be part of the package ID hash:

See:
```
  def package_id(self):
    del self.info.options.build_tests
```

Document how to specify options,
like gui, use_conan_qt,
in the caller conanfile.txt/py

# Generate dependency graph

See talk: Consistent architecture diagrams for C++ projects -  Marius Feilhauer - Meeting C++ 2019

CMake:
add_custom_target(DependencyGraph
                  COMMAND cmake --graphviz=DependencyGraph.dot
                  COMMAND dot -Tsvg DependencyGraph.dot -o DependencyGraph.svg
                  WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
)



# Doxygen

Generate doc with CMake

 - User DoxyFile
 - Dev DoxyFile
 - UML DoxyFile ?

See Doxygen & Markdown integration (and PlantUML ?)

dox subdir with a main page
Main page include the dependency graph


# Git strategy

see:
```yml
variables:
 - GIT_STRATEGY: none
```
(Not clone the git repo, for tests..)

# Other

See Clang -fdocumentation option
