/****************************************************************************
 **
 ** _Project_name_ - A C++ library to _Description_
 **
 ** Copyright (C) 2020-2020 Philippe Steinmann.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU Lesser General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public License
 ** along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **
 ****************************************************************************/
#ifndef MDT_A_QHELLO_WIDGET_H
#define MDT_A_QHELLO_WIDGET_H

#include "mdt_a_qtwidgets_export.h"
#include <QWidget>

namespace Mdt{ namespace A{

  /*! \brief Document QHelloWidget
   */
  class MDT_A_QTWIDGETS_EXPORT QHelloWidget : public QWidget
  {
   public:

    /*! \brief Document constructor
     */
    explicit QHelloWidget(QWidget * parent = nullptr);
  };

}} // namespace Mdt{ namespace A{

#endif // #ifndef MDT_A_QHELLO_WIDGET_H
