/*
 * Copyright Philippe Steinmann 2020 - 2020.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE.txt or copy at
 * https://www.boost.org/LICENSE_1_0.txt)
 */
#ifndef MDT_A_HELLO_H
#define MDT_A_HELLO_H

#include "mdt_a_export.h"

namespace Mdt{ namespace A{

  /*! \brief Document Hello
   */
  class MDT_A_EXPORT Hello
  {
   public:

    /*! \brief Document method
     */
    void execute();
  };

}} // namespace Mdt{ namespace A{

#endif // #ifndef MDT_A_HELLO_H
