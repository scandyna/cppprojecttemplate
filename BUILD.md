
This section explains how to build _Project_name_
and run the unit tests.

[[_TOC_]]

# Required tools and libraries

The tools and libraries described in [README](README.md)
are also required here.

Additional required tools and libraries that can be managed by Conan:
 - [Catch2](https://github.com/catchorg/Catch2)

If you use Conan, those additional dependencies are not required to be installed explicitly.
Otherwise, see the documentation of the dependencies.


# Project configuration using Conan

Here are the available options:

| Option           | Default | Possible Values  | Explanations |
| -----------------|:------- |:----------------:|--------------|
| shared           | True    |  [True, False]   | Build as shared library |
| use_conan_boost  | False   |  [True, False]   | Use Boost as conan dependency |
| use_conan_qt     | False   |  [True, False]   | Use [conan Qt](https://github.com/bincrafters/conan-qt) as conan dependency |

## Using Conan profiles

When using Conan for dependency management,
it is recommended to use Conan profiles.
This permits to have personal binary repository,
avoiding to recompile everything everytime.
This becomes more important if Qt is managed by Conan.

This requires modifications in the `settings.yml` Conan configuration,
and also some profile files.
See my [conan-config repository](https://gitlab.com/scandyna/conan-config) for more informations.

Some following sections will rely on Conan profiles.

# Build _Project_name_

Get _Project_name_:
```bash
git clone git@gitlab.com:scandyna/_project_name_.git
```

Create a build directory and cd to it:
```bash
mkdir build
cd build
```

## Build on Linux with the native compiler

Install the dependencies:
```bash
conan install -s build_type=Debug --build=missing ..
```

Configure _Project_name_:
```bash
cmake -DCMAKE_BUILD_TYPE=Debug -DBUILD_TESTS=ON ..
cmake-gui .
```

To build , run:
```cmd
cmake --build .
```

## Instrumented build with Gcc on Linux

Install the dependencies:
```bash
conan install --profile linux_gcc7_x86_64 -s build_type=RelWithDebInfo --build=missing ..
```

Configure _Project_name_:
```bash
cmake -DCMAKE_BUILD_TYPE=Instrumented -DBUILD_TESTS=ON -DSANITIZER_ENABLE_ADDRESS=ON -DSANITIZER_ENABLE_UNDEFINED=ON ..
cmake-gui .
```

To build , run:
```cmd
make -j4
```

## Build on Linux with Clang and libc++

Install the dependencies if Qt is used:
```bash
conan install --profile linux_clang6.0_x86_64_libc++_qt_widgets_modules -s build_type=Debug -o _Project_name_:use_conan_qt=True --build=missing ..
```

Activate the build environment:
```bash
source activate.sh
```

Configure _Project_name_:
```bash
cmake -DCMAKE_BUILD_TYPE=Debug -DBUILD_TESTS=ON ..
cmake-gui .
```

Build and run the tests:
```bash
cmake --build .
ctest . --output-on-failure
```

To restore the standard environment:
```bash
source deactivate.sh
```

## Configure and build with ThreadSanitizer

Gcc supports ThreadSanitizer, but Clang seems to give less false positive.
This is what I experieced on Ubuntu 18.04 with those default compilers.

Install the required dependencies:
```bash
conan install --profile linux_clang6.0_x86_64_libc++_tsan_qt_gui_modules -o use_conan_qt=True ..
```

Activate the build environment:
```bash
source activate.sh
```

Configure _Project_name_:
```bash
cmake -DCMAKE_BUILD_TYPE=Instrumented -DSANITIZER_ENABLE_THREAD=ON ..
cmake-gui .
```

Build and run the tests:
```bash
cmake --build . --config Instrumented
ctest . --output-on-failure -C Instrumented
```

To restore the standard environment:
```bash
source deactivate.sh
```
